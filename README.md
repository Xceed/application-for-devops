# Technical test

## Background

We want to dockerise and deploy our PHP application for production: https://bitbucket.org/Xceed/application-for-devops/src/master/. 

## Outcome

A Docker container running our PHP application in AWS.

You should be able to access into the instance through SSH.

You can use the same symfony server but you will have bonus points if the web server is NginX.

Symfony applications rely on an ENV variable called "APP_ENV" to set the environment. In this test it needs to have the value "production".

Please write the required Terraform configuration files to do this including any Dockerfiles and scripts, you can package this up as a .tar.gz to send it over.

## Comments

We don't expect you to spend a long time on this but please be descriptive in what you would have done if time allowed.

Also don't hesitate to ask any questions you might have.

