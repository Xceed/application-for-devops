<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractController
{
    public function welcome()
    {
        return new JsonResponse(["success" => true, "message" => "Nice Job!"], JsonResponse::HTTP_OK);
    }

}